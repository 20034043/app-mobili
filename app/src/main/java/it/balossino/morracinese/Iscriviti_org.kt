package it.balossino.morracinese

import android.content.ContentValues
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore

class Iscriviti_org : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_iscriviti_org)

        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val iscr = findViewById<Button>(R.id.button_iscriviti_vai_org)
        val email = findViewById<EditText>(R.id.input_mail_org).text
        val password = findViewById<EditText>(R.id.input_password_org).text
        val nome = findViewById<EditText>(R.id.input_nome_org).text
        val cogn = findViewById<EditText>(R.id.input_cognome_org).text
        val cf = findViewById<EditText>(R.id.input_cf_org).text
        val ind = findViewById<EditText>(R.id.input_ind_org).text
        val iban = findViewById<EditText>(R.id.input_iban_org).text

        //controlla che tutti i campi siano stati riempiti
        iscr.setOnClickListener {
            if(email.toString() == "" || password.toString() == "" || nome.toString() == "" || cf.toString() == "" || ind.toString() == "" || iban.toString() == "" || cogn.toString() == "" ) {
                Toast.makeText(baseContext, "Riempire tutti i campi",
                    Toast.LENGTH_LONG).show()
            }
            else {
                //Se sì, procede alla registrazione dell'utente con email e password
                auth.createUserWithEmailAndPassword(email.toString(), password.toString())
                    .addOnCompleteListener(this) { task ->

                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(ContentValues.TAG, "createUserWithEmail:success")
                            //una volta registrato l'utente, crea un oggetto organizzatore ad egli associato
                            createUser(email.toString(),cf.toString(),ind.toString(),iban.toString(),nome.toString(),cogn.toString())
                            val user = auth.currentUser
                            //poi accede alla schermata dell'organizzatore
                            updateUI(user)
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(ContentValues.TAG, "createUserWithEmail:failure", task.exception)
                            //se la registrazione non è andata a buon fine, ritorna alla schermata principale
                            updateUI(null)
                        }
                    }

            }}

    }

    //funzione per aggiornare la schermata
    private fun updateUI(user: FirebaseUser?) {
        if ( user!=null ) {
            startActivity(Intent(this@Iscriviti_org, Organizzatore::class.java))}
        else {
            Toast.makeText(baseContext, "Errore di autenticazione",
                Toast.LENGTH_LONG).show()
        }
    }

    //funzione per creare un nuovo oggetto organizzatore
    private fun createUser(email: String, cf: String, ind: String, iban: String, nome: String, cogn: String){

        val db = FirebaseFirestore.getInstance()


        // crea l'oggetto
        val entry = hashMapOf<String,Any?>(
            "email" to email,
            "codice_fiscale" to cf,
            "indirizzo_residenza" to ind,
            "iban" to iban,
            "nome" to nome,
            "cognome" to cogn
        )

        // aggiunge l'oggetto alla raccolta organizzatore
        db.collection("organizzatore")
            .add(entry)
            .addOnSuccessListener { documentReference ->
                Log.d(ContentValues.TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { e ->
                Log.w(ContentValues.TAG, "Error adding document", e)
            }

    }

}