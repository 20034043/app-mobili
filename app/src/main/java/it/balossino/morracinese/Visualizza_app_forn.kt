package it.balossino.morracinese

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class Visualizza_app_forn : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_visualizza_app_forn)

        val db = FirebaseFirestore.getInstance()
        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser
        val lista = findViewById<LinearLayout>(R.id.lista_app_forn)

        var conf = findViewById<Button>(R.id.app_conf_forn)
        var noconf = findViewById<Button>(R.id.app_noconf_forn)

        //se clicca "appuntamenti confermati", allora cerca tutti gli oggetti appuntamento associati alla mail del fornitore loggato e per ognuno,
        //se il campo "approvato" dell'appuntamento è settato a true, mostra un bottone che permette al fornitore di vedere i dettagli dell'appuntamento
        conf.setOnClickListener {
            lista.removeAllViews()
            db.collection("appuntamento").whereEqualTo("fornitore",user?.email)
                .get()
                .addOnSuccessListener { docs ->
                    for(doc in docs) {
                        if(doc.data?.get("approvato")==true){
                        val button = Button(this)
                        button.text =
                            doc.data?.get("organizzatore").toString() + " " + doc.data?.get("data")
                                .toString()
                        lista.addView(button)
                        button.setOnClickListener {
                            //se clicco il bottone, mostro l'appuntamento
                            setApp(doc.id)
                        }}
                    }
                }
        }

        //come per il bottone config ma, in questo caso, considera solo gli appuntamenti che non sono stati ancora approvati,
        //quindi con approvato = false
        noconf.setOnClickListener {
            lista.removeAllViews()
            db.collection("appuntamento").whereEqualTo("fornitore",user?.email)
                .get()
                .addOnSuccessListener { docs ->
                    for(doc in docs) {
                        if(doc.data?.get("approvato")==false){
                        val button = Button(this)
                        button.text =
                            doc.data?.get("organizzatore").toString() + " " + doc.data?.get("data")
                                .toString()
                        lista.addView(button)
                        button.setOnClickListener {
                            //se clicco il bottone, mostro l'appuntamento
                            setApp(doc.id)
                        }}
                    }
                }
        }

    }

    //funzione per andare alla schermata che mostra i dettagli dell'appuntamento
    private fun setApp(id: String) {
        val db = FirebaseFirestore.getInstance()
        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()
        val user = auth.currentUser

        //setto il campo "appuntamento corrente" del fornitore all'id dell'appuntamento associato al bottone e
        //vado alla schermata Appuntamento
        db.collection("fornitore").whereEqualTo("email",user?.email)
            .get()
            .addOnSuccessListener { documents ->
                for(document in documents){
                    db.collection("fornitore").document(document.id)
                        .update("appuntamento_corrente",id)
                    startActivity(Intent(this@Visualizza_app_forn, Appuntamento::class.java))
                }
            }
    }
}