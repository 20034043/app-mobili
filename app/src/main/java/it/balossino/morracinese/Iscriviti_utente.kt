package it.balossino.morracinese

import android.content.ContentValues.TAG
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore


class Iscriviti_utente : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_iscriviti_utente)

        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val iscr = findViewById<Button>(R.id.button_iscriviti_vai_utente)
        val email = findViewById<EditText>(R.id.input_mail_utente).text
        val password = findViewById<EditText>(R.id.input_password_utente).text
        val nome = findViewById<EditText>(R.id.input_nome_utente).text

        //coontrolla che tutti i campi siano stati riempiti
        iscr.setOnClickListener {
            if(email.toString() == "" || password.toString() == "" || nome.toString() == "") {
                Toast.makeText(baseContext, "Riempire tutti i campi",
                    Toast.LENGTH_LONG).show()
            }
            //se sì, registra l'utente usando email e password
            else {
                auth.createUserWithEmailAndPassword(email.toString(), password.toString())
                    .addOnCompleteListener(this) { task ->

                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success")
                            //una volta registrato l'utente, crea un oggetto utente a lui associato
                            createUser(email.toString(),nome.toString())
                            //poi apre la schermata utente
                            val user = auth.currentUser
                            updateUI(user)
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.exception)
                            //se la registrazione non va a buon fine, ritorna alla schermata principale
                            updateUI(null)
                        }
                    }

            }}

    }

    //funzione per aggiornare la schermata
    private fun updateUI(user: FirebaseUser?) {
        if ( user!=null ) {
            startActivity(Intent(this@Iscriviti_utente, Utente_standard::class.java))}
        else {
            Toast.makeText(baseContext, "Errore di autenticazione",
                Toast.LENGTH_LONG).show()
        }
    }

    //funzione per creare l'oggetto utente
    private fun createUser(email: String, nome: String){

        val db = FirebaseFirestore.getInstance()

        // crea l'oggetto
        val entry = hashMapOf<String,Any?>(
            "email" to email,
            "nome" to nome
        )

        // lo aggiunge alla collezione utente
        db.collection("utente")
            .add(entry)
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
            }

    }

}