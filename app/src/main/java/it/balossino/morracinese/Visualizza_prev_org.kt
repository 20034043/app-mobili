package it.balossino.morracinese

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class Visualizza_prev_org : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_visualizza_prev_forn)

        val db = FirebaseFirestore.getInstance()
        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser
        val lista = findViewById<LinearLayout>(R.id.lista_prev)

        var pagati = findViewById<Button>(R.id.prev_si)
        pagati.text = "pagati"
        var nopagati = findViewById<Button>(R.id.prev_no)
        nopagati.text = "non pagati"

        //se l'organizzatore clicca pagati, allora cerco tra tutti gli appuntamenti quelli associati alla sua mail
        //e per ognuno controllo che il campo "pagato" non sia settato a false, e che quindi sia forse (l'org ha dichiarato di avere pagato
        //il preventivo ma il fornitore non ha ancora confermato) o true (l'org ha pagato e il fornitore ha confermato)
        //nel caso il campo sia settato a true, allora colora l'appuntamento di verde per distinguerlo da quelli non ancora confermati
        pagati.setOnClickListener {
            lista.removeAllViews()
            db.collection("appuntamento").whereEqualTo("organizzatore",user?.email)
                .get()
                .addOnSuccessListener { docs ->
                    for(doc in docs) {
                        if(doc.data?.get("pagato").toString()!="false"){
                            var text = TextView(this)
                            text.text =
                                doc.data?.get("organizzatore").toString() + " " + doc.data?.get("data")
                                    .toString()+" "+doc.data?.get("preventivo").toString()+" eur"
                            text.textSize=20F
                            if(doc.data?.get("pagato")=="true") text.setTextColor(Color.parseColor("#FF327E57"))
                            lista.addView(text)
                        }
                    }
                }

            //come il bottone prima ma, in questo caso, considero solo gli appuntamenti il cui campo pagato è uguale a false,
            //ossia i cui preventivi non sono ancora stati dichiarati pagati dall'organizzatore
            nopagati.setOnClickListener {
                lista.removeAllViews()
                db.collection("appuntamento").whereEqualTo("organizzatore",user?.email)
                    .get()
                    .addOnSuccessListener { docs ->
                        for(doc in docs) {
                            if(doc.data?.get("pagato").toString()=="false"&& doc.data?.get("preventivo").toString()!="0"){
                                var text = TextView(this)
                                text.text =
                                    doc.data?.get("organizzatore").toString() + " " + doc.data?.get("data")
                                        .toString()+" "+doc.data?.get("preventivo").toString()+" eur"
                                text.textSize=20F
                                lista.addView(text)
                            }
                        }
                    }

            }}
    }}