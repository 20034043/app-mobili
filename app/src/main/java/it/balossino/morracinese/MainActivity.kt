package it.balossino.morracinese

import android.content.ContentValues.TAG
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.content.Intent
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val iscrOrg = findViewById<Button>(R.id.button_iscriviti_org)

        iscrOrg.setOnClickListener {
            startActivity(Intent(this@MainActivity, Iscriviti_org::class.java))
        }

        val iscrForn = findViewById<Button>(R.id.button_iscriviti_forn)

        iscrForn.setOnClickListener {
            startActivity(Intent(this@MainActivity, Iscriviti_forn::class.java))
        }

        val iscrUtente = findViewById<Button>(R.id.button_iscriviti_utente)

        iscrUtente.setOnClickListener {
            startActivity(Intent(this@MainActivity, Iscriviti_utente::class.java))
        }

        val acc = findViewById<Button>(R.id.button_accedi)

        //se viene cliccato il bottone "accedi", viene fatto il login dell'utente corrispondente all'email e alla password inserite
        acc.setOnClickListener {

            lateinit var auth: FirebaseAuth
            // Initialize Firebase Auth
            auth = FirebaseAuth.getInstance()

            val email = findViewById<EditText>(R.id.input_email).text
            val password = findViewById<EditText>(R.id.input_password).text

            auth.signInWithEmailAndPassword(email.toString(), password.toString())
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithEmail:success")
                        //se esiste un utente registrato con l'email e la password inserite, viene mostrata la schermata del tipo di utente corrispondente
                        val user = auth.currentUser
                        updateUI(user)
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithEmail:failure", task.exception)
                        //se non esiste un utente già registrato con quelle credenziali, viene riportato l'errore
                        updateUI(null)
                    }

                    // ...
                }


        }

    }

    //funzione per aggiornare la scermata
    private fun updateUI(user: FirebaseUser?) {

        //se non è stato trovato nessun utente, semplicemente mostra il messaggio d'errore
        if (user == null) {
            Toast.makeText(
                baseContext, "Authentication failed.",
                Toast.LENGTH_SHORT
            ).show()
        }
        //Se è stato trovato, controlla se è un fornitore
        else {
            val db = FirebaseFirestore.getInstance()
            db.collection("fornitore").whereEqualTo("email", user.email)
                .get().addOnSuccessListener { documents ->
                    //se sì, accede alla schermata fornitore
                if (!documents.isEmpty) startActivity(
                    Intent(
                        this@MainActivity,
                        Fornitore::class.java
                    )
                )
                    else{
                    //altrimenti controlla se è un organizzatore
                db.collection("organizzatore").whereEqualTo("email", user.email)
                    .get().addOnSuccessListener { documents ->
                        //se sì, accede alla schermata organizzatore
                    if (!documents.isEmpty) startActivity(
                        Intent(
                            this@MainActivity,
                            Organizzatore::class.java
                        )
                    )
                    //altrimenti vuol dire che è un utente standard e accede alla schermata corrispondente
                    else startActivity(Intent(this@MainActivity, Utente_standard::class.java))
                }}
            }
        }

    }

}
