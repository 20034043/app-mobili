package it.balossino.morracinese

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class Vetrina : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vetrina)

        val db = FirebaseFirestore.getInstance()
        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser

        val nome = findViewById<TextView>(R.id.vetrina_nome)
        val ind = findViewById<TextView>(R.id.vetrina_indirizzo)
        val pi = findViewById<TextView>(R.id.vetrina_pi)
        val descrizione = findViewById<TextView>(R.id.vetrina_descrizione)
        val email_az = findViewById<TextView>(R.id.vetrina_email)
        val orari = findViewById<TextView>(R.id.vetrina_orari)
        val prezzi = findViewById<TextView>(R.id.vetrina_prezzi)
        val app = findViewById<Button>(R.id.button_richiedi_appuntamento)

        //cerca l'oggetto organizzatore corrispondente alla mail dell'user loggato, poi accede al fornitore la cui email
        //è salvata nel campo "azienda corrente" dell'oggetto organizzatore
        db.collection("organizzatore").whereEqualTo("email", user?.email)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    db.collection("fornitore").whereEqualTo("email",document.data?.get("azienda_corrente"))
                        .get()
                        .addOnSuccessListener { docs ->
                            //una volta trovato il fornitore ne stampa i campi
                            for(it in docs){
                                nome.text = it.data?.get("nome_azienda").toString()
                                descrizione.text = it.data?.get("descrizione").toString()
                                ind.text = it.data?.get("indirizzo_azienda").toString()
                                pi.text = "P.I. : " + it.data?.get("partita_iva").toString()
                                email_az.text = "Email dell'azienda : " + it.data?.get("email_az").toString()
                                orari.text = "Orari : " + it.data?.get("orari").toString()
                                prezzi.text = "Prezzi : " + it.data?.get("prezzi").toString()
                            }
                        }
                }
            }

        app.setOnClickListener {
            startActivity(Intent(this@Vetrina, Richiedi_app::class.java))
        }

    }
}