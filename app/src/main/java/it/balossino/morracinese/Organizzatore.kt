package it.balossino.morracinese

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class Organizzatore : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_organizzatore)

        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser
        val db = FirebaseFirestore.getInstance()

        val text = findViewById<TextView>(R.id.organizzatore_title)

        //dà il benvenuto salutando l'organizzatore con il suuo nome
            val query = db.collection("organizzatore").whereEqualTo("email", user?.email)
            query.get().addOnSuccessListener { documents ->
                for ( document in documents ) text.text = "Ciao "+document.data.get("nome").toString()
            }

        val crea = findViewById<Button>(R.id.button_organizzatore_crea_evento)

        crea.setOnClickListener {
            startActivity(Intent(this@Organizzatore, Crea_evento::class.java))
        }

        val eventi = findViewById<Button>(R.id.button_organizzatore_eventi)

        eventi.setOnClickListener {
            startActivity(Intent(this@Organizzatore, Eventi::class.java))
        }

        val prev = findViewById<Button>(R.id.button_organizzatore_preventivi)

        prev.setOnClickListener {
            startActivity(Intent(this@Organizzatore, Visualizza_prev_org::class.java))
        }

    }
}