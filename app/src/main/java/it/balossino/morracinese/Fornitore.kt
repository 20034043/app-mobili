package it.balossino.morracinese

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class Fornitore : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fornitore)

        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser
        val db = FirebaseFirestore.getInstance()

        val text = findViewById<TextView>(R.id.fornitore_title)

        //dà il benvenuto al fornitore salutandolo col suo nome
            val query = db.collection("fornitore").whereEqualTo("email", user?.email)
            query.get().addOnSuccessListener { documents ->
                for ( document in documents ) text.text = "Ciao "+document.data.get("nome").toString()
            }

        var azienda = findViewById<Button>(R.id.button_fornitore_azienda)
        azienda.setOnClickListener {
            startActivity(Intent(this@Fornitore, Azienda::class.java))
        }

        var app = findViewById<Button>(R.id.button_fornitore_appuntamenti)
        app.setOnClickListener {
            startActivity(Intent(this@Fornitore, Visualizza_app_forn::class.java))
        }

        var prev = findViewById<Button>(R.id.button_fornitore_preventivi)
        prev.setOnClickListener {
            startActivity(Intent(this@Fornitore, Visualizza_prev_forn::class.java))
        }

        
    }
}