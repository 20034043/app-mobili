package it.balossino.morracinese

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.marginLeft
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class Servizi_evento : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_servizi_evento)

        val db = FirebaseFirestore.getInstance()
        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser
        val lista = findViewById<LinearLayout>(R.id.lista_servizi)

        //cerca l'evento corrente associato all'organizzatore loggato
        db.collection("organizzatore").whereEqualTo("email", user?.email)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents)
                {   db.collection("evento").document(document.data.get("evento_corrente").toString())
                    .get()
                    .addOnSuccessListener { doc ->
                        //una volta trovato l'evento, copia i servizi richiesti per quell'evento nella lista l
                        val l = doc.data?.get("servizi") as List<String>

                        //poi preleva dal db tutti i fornitori e per ognuno legge i servizi che la sua azienda offre
                        db.collection("fornitore")
                            .get()
                            .addOnSuccessListener { documents ->
                                for (document in documents) {
                                    val l2 = document.data?.get("servizi") as List<String>
                                    var i = 0
                                    //per ogni fornitore legge i servizi che offre. Al prim servizio che legge tra quelli forniti e che corrisponde a uno di quelli richiesti,
                                    // crea un bottone col nome dell'azienda del fornitore per permettere all'organizzatore di accedere all'azienda
                                    l2.forEach {
                                        if(l.contains(it.toString())){
                                            if(i==0){
                                                val button = Button(this)
                                                button.text = document.data?.get("nome_azienda").toString()
                                                lista.addView(button)
                                                button.setOnClickListener(){
                                                    //porta l'utente alla schermata dell'azienda
                                                    vaiAllAzienda(document.data?.get("email").toString())
                                                }
                                                i = 1}
                                            //per ogni servizio che corrisponde, aggiunge il nome del servizio sotto il bottone
                                            val text = TextView(this)
                                            text.text = "  "+it.toString()
                                            text.textSize = 20F
                                            lista.addView(text)
                                        }
                                    }
                                }
                            }
                    }

                }
            }

    }

    //funzione che serve a mosrtare all'organizzatore l'azienda su cui ha cliccato
    private fun vaiAllAzienda(email: String) {
        val db = FirebaseFirestore.getInstance()
        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser

        db.collection("organizzatore").whereEqualTo("email", user?.email)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    db.collection("organizzatore").document(document.id)
                        .update("azienda_corrente",email.toString())
                    startActivity(Intent(this@Servizi_evento, Vetrina::class.java))
                }
            }
    }
}