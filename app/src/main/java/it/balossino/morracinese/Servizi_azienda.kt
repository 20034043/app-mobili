package it.balossino.morracinese

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class Servizi_azienda : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_servizi_azienda)

        val db = FirebaseFirestore.getInstance()
        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser

        //setta a check tutti i box corrispondenti ai servizi che sono già stati associati all'azienda per mostrare quali sono i servizi attualmente offerti
        db.collection("fornitore").whereEqualTo("email", user?.email)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents)
                {       val l = document.data?.get("servizi") as List<String>
                        l.forEach {
                            if(it.toString() == "catering")
                            findViewById<CheckBox>(R.id.catering).isChecked = true
                            if(it.toString() == "parrucchiere")
                                findViewById<CheckBox>(R.id.parrucchiere).isChecked = true
                            if(it.toString() == "truccatore")
                                findViewById<CheckBox>(R.id.truccatore).isChecked = true
                            if(it.toString() == "band")
                                findViewById<CheckBox>(R.id.band).isChecked = true
                            if(it.toString() == "dj")
                                findViewById<CheckBox>(R.id.dj).isChecked = true
                            if(it.toString() == "fotografo")
                                findViewById<CheckBox>(R.id.fotografo).isChecked = true
                            if(it.toString() == "fioraio")
                                findViewById<CheckBox>(R.id.fioraio).isChecked = true
                            if(it.toString() == "taxi")
                                findViewById<CheckBox>(R.id.taxi).isChecked = true
                        }
                }
            }

        var modifica = findViewById<Button>(R.id.button_modifica_servizi)

        //se viene cliccato modifica, viene creato un nuovo array con solo i servizi che risultano checked e lo si sostituisce a quello presente nel campo "servizi" del fornitore
        modifica.setOnClickListener {
            val services = ArrayList<String>()

            if(findViewById<CheckBox>(R.id.catering).isChecked) services.add("catering")
            if(findViewById<CheckBox>(R.id.taxi).isChecked) services.add("taxi")
            if(findViewById<CheckBox>(R.id.fioraio).isChecked) services.add("fioraio")
            if(findViewById<CheckBox>(R.id.band).isChecked) services.add("band")
            if(findViewById<CheckBox>(R.id.dj).isChecked) services.add("dj")
            if(findViewById<CheckBox>(R.id.truccatore).isChecked) services.add("truccatore")
            if(findViewById<CheckBox>(R.id.parrucchiere).isChecked) services.add("parrucchiere")
            if(findViewById<CheckBox>(R.id.fotografo).isChecked) services.add("fotografo")


            db.collection("fornitore").whereEqualTo("email", user?.email)
                .get()
                .addOnSuccessListener { documents ->
                    for (document in documents)
                    {
                        db.collection("fornitore").document(document.id)
                        .update("servizi",services)
                        Toast.makeText(baseContext, "Modifiche apportate correttamente",
                            Toast.LENGTH_LONG).show()
                        finish()
                    }
                }
        }

    }
}