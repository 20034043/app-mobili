package it.balossino.morracinese

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class Visualizza_eventi_utente : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_visualizza_eventi_utente)

        val db = FirebaseFirestore.getInstance()
        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser
        val lista = findViewById<LinearLayout>(R.id.lista_eventi_utente)

        //cerco tutti gli eventi del db. Per ognuno, leggo la sua lista degli invitati
        //se nella lista è presente anche la mail dell'utente loggato, mostro i dettagli dell'evento
            db.collection("evento")
                .get()
                .addOnSuccessListener { docs ->
                    for(doc in docs) {
                        val l = doc.data?.get("invitati") as List<String>
                        l.forEach{
                            if(it==user?.email.toString()){
                                var text1 = TextView(this)
                                text1.text = doc.data?.get("titolo").toString()
                                text1.textSize = 25F
                                var text2 = TextView(this)
                                text2.text = doc.data?.get("tipo").toString()
                                var text3 = TextView(this)
                                text3.text = doc.data?.get("data").toString()
                                var text4 = TextView(this)
                                text4.text = doc.data?.get("organizzatore").toString()
                                var text5 = TextView(this)
                                var text6 = TextView(this)
                                text6.text = doc.data?.get("luogo").toString()
                                text5.text = ""
                                lista.addView(text1)
                                lista.addView(text2)
                                lista.addView(text3)
                                lista.addView(text6)
                                lista.addView(text4)
                                lista.addView(text5)
                            }
                    }
                }
        }

    }
}