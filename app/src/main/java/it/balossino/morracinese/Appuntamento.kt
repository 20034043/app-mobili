package it.balossino.morracinese

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType.TYPE_CLASS_NUMBER
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class Appuntamento : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_appuntamento)

        val db = FirebaseFirestore.getInstance()
        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser

        var org = findViewById<TextView>(R.id.app_org)
        var descr = findViewById<TextView>(R.id.app_descrizione)
        var data = findViewById<TextView>(R.id.app_data)

        //cerca nella collezione "fornitore" un oggetto associato alla mail dell'user
        db.collection("fornitore").whereEqualTo("email",user?.email)
            .get()
            .addOnSuccessListener { documents ->
                //se è stato trovata una corrispondenza, risale all'oggetto appuntamento tramite l'identificatore salvato nel campo "appuntamento_corrente" dell'oggetto fornitore
                for (document in documents) {
                    db.collection("appuntamento")
                        .document(document.data?.get("appuntamento_corrente").toString())
                        .get()
                        .addOnSuccessListener {
                            //una volta recuperato l'oggetto appuntamento, ne stampa i campi
                            org.text = it.data?.get("organizzatore").toString()
                            descr.text = it.data?.get("descrizione").toString()
                            data.text = it.data?.get("data").toString()
                            var button = Button(this)
                            //se non è ancora stato approvato, crea un bottone tramite cui il fornitore può confermarlo
                            if (it.data?.get("approvato") == false) {
                                button.text = "Conferma"
                                findViewById<LinearLayout>(R.id.app_layout).addView(button)
                                button.setOnClickListener {
                                    db.collection("appuntamento").document(
                                        document.data?.get("appuntamento_corrente").toString()
                                    )
                                        .update("approvato", true)
                                    Toast.makeText(
                                        baseContext, "Appuntamento confermato",
                                        Toast.LENGTH_LONG
                                    ).show()
                                    finish()
                                }
                            } //se invece è stato approvato ma non è ancora stato associato un preventivo, crea un edit text tramite cui il fornitore può inserirne uno
                            else if(it.data?.get("preventivo").toString()=="0"){
                                var text = TextView(this)
                                text.text = "Associa un preventivo (euro)"
                                var input = EditText(this)
                                input.inputType = TYPE_CLASS_NUMBER
                                button.text = "Associa"
                                findViewById<LinearLayout>(R.id.app_layout).addView(text)
                                findViewById<LinearLayout>(R.id.app_layout).addView(input)
                                findViewById<LinearLayout>(R.id.app_layout).addView(button)
                                button.setOnClickListener {
                                    if(input.text.toString()==""){
                                        Toast.makeText(
                                            baseContext, "Inserire una cifra",
                                            Toast.LENGTH_LONG
                                        ).show()
                                    }
                                    else{
                                        db.collection("appuntamento")
                                            .document(document.data?.get("appuntamento_corrente").toString())
                                            .update("preventivo",Integer.parseInt(input.text.toString()))
                                        finish()
                                    }
                                }
                            }
                            //se invece l'appuntamento è stato approvato e vi è già stato associato un preventivo, stampa il preventivo
                            else{
                                var text = TextView(this)
                                text.text = "preventivo : "+it.data?.get("preventivo").toString()+" eur"
                                text.textSize = 20F
                                findViewById<LinearLayout>(R.id.app_layout).addView(text)
                                //se il preventivo è stato settato come "pagato" dall'organizzatore ma il fornitore non l'ha ancora confermato, crea un bottone tramite cui può confermarlo
                                if(it.data?.get("pagato").toString()=="forse"){
                                    button.text = "Conferma avvenuto pagamento"
                                    findViewById<LinearLayout>(R.id.app_layout).addView(button)
                                    button.setOnClickListener {
                                        db.collection("appuntamento")
                                            .document(document.data?.get("appuntamento_corrente").toString())
                                            .update("pagato","true")
                                        Toast.makeText(
                                            baseContext, "Pagamento confermato",
                                            Toast.LENGTH_LONG
                                        ).show()
                                        finish()
                                    }
                                }
                            }
                        }
                }
            }

        //cerca nella collezione "organizzatore" un oggetto associato alla mail dell'user
        db.collection("organizzatore").whereEqualTo("email",user?.email)
            .get()
            .addOnSuccessListener { documents ->
                //se è stato trovata una corrispondenza, risale all'oggetto appuntamento tramite l'identificatore salvato nel campo "appuntamento_corrente" dell'oggetto organizzatore
                for (document in documents) {
                    db.collection("appuntamento")
                        .document(document.data?.get("appuntamento_corrente").toString())
                        .get()
                        .addOnSuccessListener { doc->
                            //una volta recuperato l'oggetto appuntamento, ne stampa i campi
                            org.text = doc.data?.get("fornitore").toString()
                            descr.text = doc.data?.get("descrizione").toString()
                            data.text = doc.data?.get("data").toString()

                            //se è già stato associato un preventivo lo stampa
                            if(doc.data?.get("preventivo").toString()!="0"){
                                var text = TextView(this)
                                text.text = "preventivo : "+doc.data?.get("preventivo").toString()+" eur"
                                text.textSize = 20F
                                findViewById<LinearLayout>(R.id.app_layout).addView(text)

                                //se l'organizzatore non ne ha ancora confermato il pagamento, crea un bottone tramite cui può confermarlo
                                if(doc.data?.get("pagato").toString()=="false") {
                                    var button = Button(this)
                                    button.text = "Conferma pagamento"
                                    findViewById<LinearLayout>(R.id.app_layout).addView(button)
                                    button.setOnClickListener {
                                        //se l'organizzatore conferma il pagamento, viene aggiornato lo stato del pagamento e il budget dell'organizzatore per l'evento
                                        var costo = Integer.parseInt(doc.data?.get("preventivo").toString())
                                        db.collection("evento").document(document.data.get("evento_corrente").toString())
                                            .get()
                                            .addOnSuccessListener {
                                                db.collection("appuntamento")
                                                    .document(document.data?.get("appuntamento_corrente").toString())
                                                    .update("pagato","forse")
                                                var budget = Integer.parseInt(it.data?.get("budget").toString())-costo
                                                db.collection("evento").document(document.data?.get("evento_corrente").toString())
                                                    .update("budget",budget)
                                                Toast.makeText(
                                                    baseContext, "Pagamento confermato",
                                                    Toast.LENGTH_LONG
                                                ).show()
                                                finish()
                                            }
                                    }
                                }
                            }
                        }
                }
            }

    }
}