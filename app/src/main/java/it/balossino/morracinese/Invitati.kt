package it.balossino.morracinese

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore

class Invitati : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invitati)

        val db = FirebaseFirestore.getInstance()
        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser
        val lista = findViewById<LinearLayout>(R.id.lista_invitati)

        //cerca l'oggetto organizzatore associato all'utente loggato, poi cerca l'evento scelto di cui vedere gli invitati accedendo al campo evento corrente dell'oggetto organizzatore trovato
        db.collection("organizzatore").whereEqualTo("email", user?.email)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents)
                {   db.collection("evento").document(document.data.get("evento_corrente").toString())
                    .get()
                    .addOnSuccessListener { doc ->
                        //trovato l'oggetto evento, accede al campo "invitati" in cui sono memorizzate le email degli invitati e stampa ogni elemento dell'array
                        val l = doc.data?.get("invitati") as List<String>
                        l.forEach {
                            val text = TextView(this)
                            text.text = it.toString()
                            text.textSize = 25F
                            lista.addView(text)
                        }
                    }

                }
            }

        val invitato = findViewById<EditText>(R.id.input_invitato).text
        val invita = findViewById<Button>(R.id.button_aggiungi_invitato)

        //se l'organizzatore clicca il bottone "invita", controlla che sia stata inserita una email
        invita.setOnClickListener {

            if(invitato.toString() == "") {
                    Toast.makeText(baseContext, "Inserire una email",
                        Toast.LENGTH_LONG).show()
            }

            //se sì, cerca l'evento corrente nello stesso modo di prima
            else { db.collection("organizzatore").whereEqualTo("email", user?.email)
                .get()
                .addOnSuccessListener { documents ->
                    for (document in documents) {
                        val ev = db.collection("evento")
                            .document(document.data.get("evento_corrente").toString())
                        ev.get().addOnSuccessListener { doc ->
                            //una volta trovato l'oggetto evento, controlla che l'utente inserito non sia già stato invitato leggendo la lista degli invitati
                                    val l = doc.data?.get("invitati") as List<String>
                                    var i = 0
                                    l.forEach{
                                        if ( it.toString() == invitato.toString() ){
                                            Toast.makeText(baseContext, "Hai già invitato questa persona",
                                                Toast.LENGTH_LONG).show()
                                            i = 1
                                        }
                                    }
                                if(i==0){
                                    //se non è stato trovato un altro invitato con la stessa mail, aggiunge la mail all'elenco degli invitati
                                    ev.update("invitati", FieldValue.arrayUnion(invitato.toString()))
                                    Toast.makeText(baseContext, "Invito correttamente spedito",
                                        Toast.LENGTH_LONG).show()
                                }
                            }
                    }
                }

        }

    }}}