package it.balossino.morracinese

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class Visualizza_prev_forn : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_visualizza_prev_forn)

        val db = FirebaseFirestore.getInstance()
        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser
        val lista = findViewById<LinearLayout>(R.id.lista_prev)

        var confermati = findViewById<Button>(R.id.prev_si)
        confermati.text = "confermati"
        var noconfermati = findViewById<Button>(R.id.prev_no)
        noconfermati.text = "non confermati"

        //se il fornitore clicca confermati, allora cerco tra tutti gli appuntamenti quelli associati alla sua mail
        //e per ognuno controllo che il campo "pagato" sia settato a true. Se si, mostro i dettagli del preventivo
        confermati.setOnClickListener {
            lista.removeAllViews()
            db.collection("appuntamento").whereEqualTo("fornitore",user?.email)
                .get()
                .addOnSuccessListener { docs ->
                    for(doc in docs) {
                        if(doc.data?.get("pagato").toString()=="true"){
                            var text = TextView(this)
                            text.text =
                                doc.data?.get("organizzatore").toString() + " " + doc.data?.get("data")
                                    .toString()+" "+doc.data?.get("preventivo").toString()+" eur"
                            text.textSize=20F
                            text.setTextColor(Color.parseColor("#FF327E57"))
                            lista.addView(text)
                    }
                }
        }

            //come per il bottone precedente ma, in questo caso, considero solo gli appuntamenti il cui pagamento è false (non ancora pagati)
            //o forse (l'organizzatore ha dichiarato di avere pagato ma il fornitore non ha ancora confermato il pagamento)
            //se lo stato del pagamento è forse, allora mostro l'appuntamento in verde per distinguerlo da quelli che invece non sono ancora stati pagati
        noconfermati.setOnClickListener {
            lista.removeAllViews()
            db.collection("appuntamento").whereEqualTo("fornitore",user?.email)
                .get()
                .addOnSuccessListener { docs ->
                    for(doc in docs) {
                        if(doc.data?.get("pagato").toString()!="true" && doc.data?.get("preventivo").toString()!="0"){
                            var text = TextView(this)
                            text.text =
                                doc.data?.get("organizzatore").toString() + " " + doc.data?.get("data")
                                    .toString()+" "+doc.data?.get("preventivo").toString()+" eur"
                            text.textSize=20F
                            if(doc.data?.get("pagato").toString()=="forse") text.setTextColor(Color.parseColor("#FF327E57"))
                            lista.addView(text)
                    }
                }
        }

    }}
}}