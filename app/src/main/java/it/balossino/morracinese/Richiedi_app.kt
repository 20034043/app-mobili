package it.balossino.morracinese

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class Richiedi_app : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_richiedi_app)

        val db = FirebaseFirestore.getInstance()
        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser

        val descr = findViewById<EditText>(R.id.input_app_descrizione).text
        val data = findViewById<EditText>(R.id.input_app_data).text

        val richiedi = findViewById<Button>(R.id.button_richiedi_app_vai)

        //se viene cliccato il bottone "richiedi appuntamento" controlla che sia stata inserita la data per cui lo richiede
        richiedi.setOnClickListener {
            if(data.toString()==""){
                Toast.makeText(baseContext, "Inserire una data",
                    Toast.LENGTH_LONG).show()
            }
            //se sì, viene creato un nuovo oggetto appuntamento e inserito nel db
            else {
                db.collection("organizzatore").whereEqualTo("email", user?.email)
                    .get()
                    .addOnSuccessListener { documents ->
                        for (doc in documents) {
                            val entry = hashMapOf<String, Any?>(
                                "fornitore" to doc.data?.get("azienda_corrente").toString(),
                                "organizzatore" to user?.email.toString(),
                                "descrizione" to descr.toString(),
                                "data" to data.toString(),
                                "approvato" to false,
                                "preventivo" to 0,
                                "pagato" to "false"
                            )
                            db.collection("appuntamento")
                                .add(entry)
                                .addOnSuccessListener {
                                    Toast.makeText(baseContext, "Richiesta di appuntamento inviata",
                                        Toast.LENGTH_LONG).show()
                                    finish()
                                }
                        }
                    }
            }
        }

    }
}