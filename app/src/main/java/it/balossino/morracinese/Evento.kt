package it.balossino.morracinese

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class Evento : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_evento)

        val db = FirebaseFirestore.getInstance()
        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser

        val titolo = findViewById<TextView>(R.id.evento_title)
        val tipo = findViewById<TextView>(R.id.evento_type)
        val data = findViewById<TextView>(R.id.evento_date)
        val luogo = findViewById<TextView>(R.id.evento_place)
        val budget = findViewById<TextView>(R.id.evento_budget_restante)

        //cerca l'evento scelto da mostrare nel campo "evento_corrente" dell'oggetto organizzatore associato alla mail dell'utente loggato
        db.collection("organizzatore").whereEqualTo("email", user?.email)
            .get()
            .addOnSuccessListener { documents ->
                //trovato l'evento, ne stampa i campi
                for (document in documents)
                {   db.collection("evento").document(document.data.get("evento_corrente").toString())
                    .get().addOnSuccessListener { document ->

                                titolo.text = document.data?.get("titolo").toString()
                                data.text = document.data?.get("data").toString()
                                luogo.text = document.data?.get("luogo").toString()
                                budget.text = "Budget restante: "+document.data?.get("budget").toString()+" eur"
                                tipo.text = document.data?.get("tipo").toString()
                    }
                }
            }

        val invitati = findViewById<Button>(R.id.button_evento_invitati)
        val servizi = findViewById<Button>(R.id.button_evento_servizi)
        val appuntamenti = findViewById<Button>(R.id.button_evento_appuntamenti)

        invitati.setOnClickListener {
            startActivity(Intent(this@Evento, Invitati::class.java))
        }

        servizi.setOnClickListener {
            startActivity(Intent(this@Evento, Servizi_evento::class.java))
        }

        appuntamenti.setOnClickListener {
            startActivity(Intent(this@Evento, Visualizza_app_org::class.java))
        }

    }
}