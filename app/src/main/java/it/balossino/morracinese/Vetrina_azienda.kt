package it.balossino.morracinese

import android.content.ContentValues
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions

class Vetrina_azienda : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vetrina_azienda)

        val db = FirebaseFirestore.getInstance()
        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser

        val descrizione = findViewById<EditText>(R.id.input_vetrina_descrizione).text
        val email_az = findViewById<EditText>(R.id.input_vetrina_mail).text
        val orari = findViewById<EditText>(R.id.input_vetrina_orari).text
        val prezzi = findViewById<EditText>(R.id.input_vetrina_prezzi).text
        val ind = findViewById<EditText>(R.id.input_vetrina_ind).text
        val iban = findViewById<EditText>(R.id.input_vetrina_iban).text
        val pi = findViewById<EditText>(R.id.input_vetrina_pi).text

        val aggiorna = findViewById<Button>(R.id.button_aggiorna_vetrina_vai)

        //se clicca aggiorna, allora cerca l'oggetto fornitore associato all'user loggato
        aggiorna.setOnClickListener {

            db.collection("fornitore").whereEqualTo("email",user?.email)
                .get()
                .addOnSuccessListener { documents ->
                    //una volta trovato l'oggetto, ne aggiorna i campi con quelli presi dagli edit text
                    //a meno che nell'edit text non sia stato digitato nulla. In quel caso lascia il campo invariato
                    for(document in documents) {
                        var idd = document.id
                        if(descrizione.toString()!=""){
                        db.collection("fornitore").document(idd)
                            .update("descrizione", descrizione.toString())}
                        if(email_az.toString()!=""){
                            db.collection("fornitore").document(idd)
                                .update("email_az", email_az.toString())}
                        if(orari.toString()!=""){
                            db.collection("fornitore").document(idd)
                                .update("orari", orari.toString())}
                        if(prezzi.toString()!=""){
                            db.collection("fornitore").document(idd)
                                .update("prezzi", prezzi.toString())}
                        if(ind.toString()!=""){
                            db.collection("fornitore").document(idd)
                                .update("indirizzo_azienda", ind.toString())}
                        if(iban.toString()!=""){
                            db.collection("fornitore").document(idd)
                                .update("iban", iban.toString())}
                        if(pi.toString()!=""){
                            db.collection("fornitore").document(idd)
                                .update("partita_iva", pi.toString())}
                            Toast.makeText(baseContext, "Modifiche apportate correttamente",
                                Toast.LENGTH_LONG).show()
                        startActivity(
                            Intent(
                                this@Vetrina_azienda,
                                Fornitore::class.java
                            ))
                }
            }
        }
    }
}
