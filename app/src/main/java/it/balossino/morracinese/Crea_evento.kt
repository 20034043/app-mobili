package it.balossino.morracinese

import android.content.ContentValues
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class Crea_evento : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crea_evento)

        val crea = findViewById<Button>(R.id.button_crea_evento_vai)
        val tipo = findViewById<EditText>(R.id.input_event_type).text
        val data = findViewById<EditText>(R.id.input_event_date).text
        val luogo = findViewById<EditText>(R.id.input_event_place).text
        val budget = findViewById<EditText>(R.id.input_event_budget).text
        val titolo = findViewById<EditText>(R.id.input_event_title).text

        //controlla che tutti i campi siano stati riempiti
        crea.setOnClickListener {
            if(tipo.toString() == "" || data.toString() == "" || luogo.toString() == "" || budget.toString() == "" || titolo.toString() == ""){
                Toast.makeText(baseContext, "Riempire tutti i campi",
                    Toast.LENGTH_LONG).show()
            }
            else {
                //se sì, crea l'evento e mostra i servizi tra cui l'organizzatore può scegliere
                creaEvento(tipo.toString(),data.toString(),luogo.toString(),budget.toString(), titolo.toString())
                startActivity(Intent(this@Crea_evento, Scelta_servizi::class.java))
            }
        }

    }

    //funzione per creare l'evento
    private fun creaEvento(tipo: String, data: String, luogo: String, budget: String, titolo: String) {

        val db = FirebaseFirestore.getInstance()
        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser
        if(user == null ) return

        var list = ArrayList<String>()

        //Oggetto evento
        var entry = hashMapOf<String,Any?>(
            "tipo" to tipo,
            "data" to data,
            "luogo" to luogo,
            "budget" to budget,
            "organizzatore" to user.email.toString(),
            "titolo" to titolo,
            "invitati" to list
        )

        //aggiunge l'evento al db e associa l'evento creato all'organizzatore corrente nel campo "evento corrente"
        db.collection("evento")
            .add(entry)
            .addOnSuccessListener { documentReference ->
                Log.d(ContentValues.TAG, "DocumentSnapshot added with ID: ${documentReference.id}")

                db.collection("organizzatore").whereEqualTo("email", user.email)
                    .get()
                    .addOnSuccessListener { documents ->
                        var idd = ""
                        for (document in documents) idd = document.id
                        db.collection("organizzatore").document(idd)
                            .update("evento_corrente",documentReference.id)
                    }
            }

    }

}