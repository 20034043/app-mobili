package it.balossino.morracinese

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class Utente_standard : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_utente_standard)

        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser
        val db = FirebaseFirestore.getInstance()

        val text = findViewById<TextView>(R.id.utente_title)

        //cerca l'oggetto utente corrispondente alla mail dell'user loggato e gli da il benvenuto col suo nome
            val query = db.collection("utente").whereEqualTo("email", user?.email)
            query.get().addOnSuccessListener { documents ->
                for ( document in documents ) text.text = "Ciao "+document.data.get("nome").toString()
            }


        val eventi = findViewById<Button>(R.id.button_utente_eventi)

        eventi.setOnClickListener {
            startActivity(Intent(this@Utente_standard, Visualizza_eventi_utente::class.java))
        }

    }
}