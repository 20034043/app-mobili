package it.balossino.morracinese

import android.content.ContentValues.TAG
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore


class Iscriviti_forn : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_iscriviti_forn)

        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val iscr = findViewById<Button>(R.id.button_iscriviti_vai_forn)
        val email = findViewById<EditText>(R.id.input_mail_forn).text
        val password = findViewById<EditText>(R.id.input_password_forn).text
        val nomeAz = findViewById<EditText>(R.id.input_nomeAz_forn).text
        val pi = findViewById<EditText>(R.id.input_pi_forn).text
        val indAz = findViewById<EditText>(R.id.input_indAz_forn).text
        val iban = findViewById<EditText>(R.id.input_iban_forn).text
        val nome = findViewById<EditText>(R.id.input_nome_forn).text
        val cogn = findViewById<EditText>(R.id.input_cognome_forn).text

        //controlla che tutti i campi siano stati riempiti
        iscr.setOnClickListener {
            if(email.toString() == "" || password.toString() == "" || nomeAz.toString() == "" || pi.toString() == "" || indAz.toString() == "" || iban.toString() == "" || nome.toString() == "" || cogn.toString() == "" ) {
                Toast.makeText(baseContext, "Riempire tutti i campi",
                    Toast.LENGTH_LONG).show()
            }
            //se si, procede alla registrazione dell'utente con mail e password
            else {
            auth.createUserWithEmailAndPassword(email.toString(), password.toString())
                .addOnCompleteListener(this) { task ->

                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "createUserWithEmail:success")
                        //una volta registrato l'utente, crea un'oggetto "fornitore" a lui associato
                        createUser(email.toString(),nomeAz.toString(),pi.toString(),indAz.toString(),iban.toString(),nome.toString(),cogn.toString())
                        val user = auth.currentUser
                        //poi accede alla schermata del fornitore
                        updateUI(user)
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "createUserWithEmail:failure", task.exception)
                        updateUI(null)
                    }
                }

        }}

    }

    //funzione per accedere alla schermata del fornitore
    private fun updateUI(user: FirebaseUser?) {
        if ( user!=null ) {
            startActivity(Intent(this@Iscriviti_forn, Fornitore::class.java))}
        else {
        Toast.makeText(baseContext, "Errore di autenticazione",
            Toast.LENGTH_LONG).show()
        }
    }

    //funzione per creare un oggetto "fornitore" associato all'utente appena registrato
    private fun createUser(email: String, nomeAz: String, pi: String, indAz: String, iban: String, nome: String, cogn: String){

        val db = FirebaseFirestore.getInstance()

        // Crea l'oggetto fornitore
        val entry = hashMapOf<String,Any?>(
            "email" to email,
            "nome_azienda" to nomeAz,
            "partita_iva" to pi,
            "indirizzo_azienda" to indAz,
            "iban" to iban,
            "nome" to nome,
            "cognome" to cogn,
            "servizi" to ArrayList<String>()
        )

        // aggiunge l'oggetto alla collezione fornitore
        db.collection("fornitore")
            .add(entry)
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
            }

    }

}