package it.balossino.morracinese

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class Eventi : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_eventi)

        val db = FirebaseFirestore.getInstance()
        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser
        val lista = findViewById<LinearLayout>(R.id.lista_eventi)

        //cerca tutti gli eventi associati alla mail dell'organizzatore loggato
        db.collection("evento").whereEqualTo("organizzatore", user?.email)
            .get()
            .addOnSuccessListener { documents ->
                //per ogni evento trovato crea un bottone su cui cliccare per andare ai dettagli dell'evento
                for (document in documents)
                {
                    val button = Button(this)
                    button.text = document.data.get("titolo").toString()
                    //se viene cliccato un evento, viene settato l'evento come "evento corrente" dell'oggetto organizzatore corrispondente all'utente loggato e si mostra l'evento scelto
                    button.setOnClickListener(){
                        setEventoCorrente(document.id)
                    }
                    lista.addView(button)

                }
            }

    }

    //funzione per settare l'evento scelto come evento corrente e mostrarlo
    private fun setEventoCorrente(id: String){
        val db = FirebaseFirestore.getInstance()
        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser

        db.collection("organizzatore").whereEqualTo("email", user?.email)
            .get()
            .addOnSuccessListener { documents ->
                var idd = ""
                for (document in documents) idd = document.id
                db.collection("organizzatore").document(idd)
                    .update("evento_corrente",id)
                    .addOnSuccessListener {
                        startActivity(Intent(this@Eventi, Evento::class.java)) }
            }
    }
}