package it.balossino.morracinese

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class Azienda : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_azienda)

        val db = FirebaseFirestore.getInstance()
        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser

        val nome = findViewById<TextView>(R.id.aziena_nome)
        val ind = findViewById<TextView>(R.id.azienda_indirizzo)
        val pi = findViewById<TextView>(R.id.azienda_pi)
        val email = findViewById<TextView>(R.id.azienda_email_forn)
        val iban = findViewById<TextView>(R.id.azienda_iban)
        val descrizione = findViewById<TextView>(R.id.azienda_descrizione)
        val email_az = findViewById<TextView>(R.id.azienda_email)
        val orari = findViewById<TextView>(R.id.azienda_orari)
        val prezzi = findViewById<TextView>(R.id.azienda_prezzi)

        //risale all'oggetto fornitore associato alla mail dell'user, poi ne stampa i campi
        db.collection("fornitore").whereEqualTo("email", user?.email)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents)
                {       nome.text = document.data?.get("nome_azienda").toString()
                        if(document.data?.get("descrizione").toString()=="null") descrizione.text = "Non hai ancora inserito una descrizione"
                        else descrizione.text = document.data?.get("descrizione").toString()
                        ind.text = document.data?.get("indirizzo_azienda").toString()
                        pi.text = "P.I. : "+document.data?.get("partita_iva").toString()
                        email.text = "La tua email : "+document.data?.get("email").toString()
                        if(document.data?.get("email_az").toString()=="null") email_az.text = "Non hai ancora inserito un'email aziendale"
                        else email_az.text = "L'email della tua azienda : "+document.data?.get("email_az").toString()
                        iban.text = "Iban : "+document.data?.get("iban").toString()
                        if(document.data?.get("orari").toString()=="null") orari.text = "Non hai ancora inserito un orario"
                        else orari.text = "Orari : "+document.data?.get("orari").toString()
                        if(document.data?.get("prezzi").toString()=="null") prezzi.text = "Non hai ancora inserito i prezzi"
                        else prezzi.text = "Prezzi : "+document.data?.get("prezzi").toString()
                }
            }

        var servizi = findViewById<Button>(R.id.azienda_servizi)

        servizi.setOnClickListener {
            startActivity(Intent(this@Azienda, Servizi_azienda::class.java))
        }

        var vetrina = findViewById<Button>(R.id.azienda_vetrina)

        vetrina.setOnClickListener {
            startActivity(Intent(this@Azienda, Vetrina_azienda::class.java))
        }

    }
}