package it.balossino.morracinese

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class Scelta_servizi : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scelta_servizi)

        val prosegui = findViewById<Button>(R.id.button_scelta_servizi_vai)

        var catering = findViewById<CheckBox>(R.id.radio_catering)
        val taxi = findViewById<CheckBox>(R.id.radio_servizioTaxi)
        val fioraio = findViewById<CheckBox>(R.id.radio_fioraio)
        val band = findViewById<CheckBox>(R.id.radio_Band)
        val dj = findViewById<CheckBox>(R.id.radio_dj)
        val truccatore = findViewById<CheckBox>(R.id.radio_truccatore)
        val parrucchiere = findViewById<CheckBox>(R.id.radio_parrucchiere)
        val fotografo = findViewById<CheckBox>(R.id.radio_fotografo)

        //una volta cliccato procedi, vengono aggiornati i servizi in base alle scelte dell'utente e viene mostrata la schermata dell'evento
        prosegui.setOnClickListener {
                aggiungiServizi(catering.isChecked,taxi.isChecked,fioraio.isChecked,band.isChecked,dj.isChecked,truccatore.isChecked,parrucchiere.isChecked,fotografo.isChecked)
                startActivity(Intent(this@Scelta_servizi, Evento::class.java))
        }

    }

    //funzione per aggiornare i servizi richiesti per l'evento
    private fun aggiungiServizi(catering: Boolean,taxi: Boolean,fioraio: Boolean,band: Boolean,dj: Boolean,truccatore: Boolean,parrucchiere: Boolean,fotografo: Boolean) {

        val db = FirebaseFirestore.getInstance()
        lateinit var auth: FirebaseAuth
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser

        //crea un array in cui inserisce il nome sotto forma di stringa dei servizi scelti, che andrà poi a inserire nel campo "servizi" dell'evento
        val services = ArrayList<String>()

        if(catering) services.add("catering")
        if(taxi) services.add("taxi")
        if(fioraio) services.add("fioraio")
        if(band) services.add("band")
        if(dj) services.add("dj")
        if(truccatore) services.add("truccatore")
        if(parrucchiere) services.add("parrucchiere")
        if(fotografo) services.add("fotografo")


        db.collection("organizzatore").whereEqualTo("email", user?.email)
                    .get()
                    .addOnSuccessListener { documents ->
                        for (document in documents)
                        {   db.collection("evento").document(document.data.get("evento_corrente").toString())
                            .update("servizi",services) }
                    }

    }
}